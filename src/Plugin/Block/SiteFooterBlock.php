<?php

namespace Drupal\uw_ct_site_footer\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\LocalTaskManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\uw_cfg_common\Service\UWServiceInterface;
use Drupal\uw_ct_site_footer\Service\UWBrandLogoInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a site footer Block.
 *
 * @Block(
 *   id = "uw_block_site_footer",
 *   admin_label = @Translation("Site Footer block"),
 * )
 */
class SiteFooterBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPathStack;

  /**
   * Route matcher.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Current user account proxy.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $accountProxy;

  /**
   * Local task manager.
   *
   * @var \Drupal\Core\Menu\LocalTaskManagerInterface
   */
  protected $localTaskManager;

  /**
   * Client HTTP factory.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $clientFactory;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The UW Service.
   *
   * @var \Drupal\uw_cfg_common\Service\UWServiceInterface
   */
  protected $uwService;

  /**
   * Brand logo service for site footer.
   *
   * @var \Drupal\uw_ct_site_footer\Service\UWBrandLogoInterface
   */
  protected UWBrandLogoInterface $brandLogo;

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Query to get the vid of the site footer node.
    $query = $this->database->select('node', 'n');
    $query->addField('n', 'nid');
    $query->addField('n', 'vid');
    $query->condition('n.type', 'uw_ct_site_footer');
    $results = $query->execute()->fetchAll();

    // Get the nid and vid, since there is only one,
    // we can get the first result.
    $vid = $results[0]->vid ?? NULL;
    $nid = $results[0]->nid ?? NULL;

    // Variable to store info about the site footer.
    $site_footer = [];

    // If there is uw_ct_site_footer node existing, set up $markup.
    if ($vid && $nid) {

      // Get the current path of the node.
      $current_path = $this->currentPathStack->getPath();

      // Get the URL parts from the current path.
      $url = explode('/', $current_path);

      // Check if we are on a revision of the site footer.
      if (
        in_array('revisions', $url) &&
        in_array($nid, $url) &&
        end($url) == 'view'
      ) {

        // Ensure that the user can view revisions for the site footer.
        if (!$this->accountProxy->hasPermission('view uw_ct_site_footer revisions')) {
          return [];
        }

        // Load the node based on the revision.
        $node = $this->entityTypeManager->getStorage('node')
          ->loadRevision($url[4]);
      }
      // If we are on a site footer node, check if on
      // the latest revision or just the view tab, and
      // load the appropriate node.
      elseif (
        in_array('node', $url) &&
        in_array($nid, $url)
      ) {

        // Ensure that the user can view revisions for the site footer.
        if (!$this->accountProxy->hasPermission('view uw_ct_site_footer revisions')) {
          return [];
        }

        // If we are on the latest or layout tab,
        // load the latest revision.
        if (
          end($url) == 'latest' ||
          end($url) == 'layout'
        ) {

          // Get the latest revision (vid) for the node.
          $vid = $this->entityTypeManager->getStorage('node')->getLatestRevisionId($nid);

          // Load the node based on the latest revision.
          $node = $this->entityTypeManager->getStorage('node')
            ->loadRevision($vid);
        }
        // We are on the view tab, just load the node.
        else {

          // Load the node.
          $node = $this->entityTypeManager->getStorage('node')
            ->load($nid);
        }
      }
      // If we get here, not on a site footer node.
      // Now check that the site footer is published.
      else {

        // Get the node.
        $node = $this->entityTypeManager->getStorage('node')->loadRevision($vid);

        // Ensure that the site footer is published.
        if ($node->moderation_state->value !== 'published') {
          return [];
        }
      }

      // Ensure that we have a array for the layout builder.
      $layout = [];

      // Load in the layout sections.
      $sections = $node->layout_builder__layout->getSections();

      // Step through each section and get the render array.
      foreach ($sections as $section) {

        // Set the render array of the section.
        $layout[] = $section->toRenderArray();
      }

      // Set the body to the layout sections.
      $site_footer['content'] = $layout;

      // If there is an instagram element, add it to the
      // site footer array.
      if (isset($node->get('field_site_footer_instagram')->getValue()[0]['value'])) {
        $site_footer['social_media']['menu_tree'][] = [
          "text" => "Instagram",
          "url" => 'https://instagram.com/' . $node->get('field_site_footer_instagram')->getValue()[0]['value'],
        ];
      }

      // If there is a X (formerly Twitter) element, add it to the
      // site footer array.
      // Note the "text" MUST remain "Twitter" for the site footer to work.
      if (isset($node->get('field_site_footer_twitter')->getValue()[0]['value'])) {
        $site_footer['social_media']['menu_tree'][] = [
          "text" => "Twitter",
          "url" => "https://www.twitter.com/" . $node->get('field_site_footer_twitter')->getValue()[0]['value'],
        ];
      }

      // If there is a LinkedIn element, add it to the
      // site footer array.
      if (isset($node->get('field_site_footer_linked_in')->getValue()[0]['value'])) {
        $site_footer['social_media']['menu_tree'][] = [
          "text" => "LinkedIn",
          "url" => 'https://linkedin.com/' . $node->get('field_site_footer_linked_in')->getValue()[0]['value'],
        ];
      }

      // If there is a facebook element, add it to the
      // site footer array.
      if (isset($node->get('field_site_footer_facebook')->getValue()[0]['value'])) {
        $site_footer['social_media']['menu_tree'][] = [
          "text" => "Facebook",
          "url" => "https://www.facebook.com/" . $node->get('field_site_footer_facebook')->getValue()[0]['value'],
        ];
      }

      // If there is a YouTube element, add it to the
      // site footer array.
      if (isset($node->get('field_site_footer_you_tube')->getValue()[0]['value'])) {
        $site_footer['social_media']['menu_tree'][] = [
          'text' => 'Youtube',
          'url' => 'https://youtube.com/' . $node->get('field_site_footer_you_tube')->getValue()[0]['value'],
        ];
      }

      // If there is a snapchat element, add it to the
      // site footer array.
      if (isset($node->get('field_site_footer_snapchat')->getValue()[0]['value'])) {
        $site_footer['social_media']['menu_tree'][] = [
          "text" => "Snapchat",
          "url" => "https://www.snapchat.com/add/" . $node->get('field_site_footer_snapchat')->getValue()[0]['value'],
        ];
      }

      // Get the site footer logo.
      // If somehow the choice has never been made, select the no logo default.
      $field_site_footer_logo = $node->get('field_site_footer_logo')->getValue()[0]['value'] ?? 0;

      // Get logo field value if the field is not empty.
      if ($field_site_footer_logo !== "0") {

        // Get logo name.
        $site_footer['logo_name'] = $field_site_footer_logo;

        // When select a certain logo.
        if ($site_footer['logo_name'] !== '1') {
          // Check brand site for logo, if logo is found, create a local copy.
          // If brand site logo request fails, try to use stored local copy.
          $logo = $this->brandLogo->getSiteFooterLogo($site_footer['logo_name']);

          // If logo is found, merge arrays, using spread operator, which should
          // be faster than function call array_merge.
          if (!empty($logo)) {
            $site_footer = [...$site_footer, ...$logo];
          }
          // If brand site failed to provide logo, and there is no local copy
          // of logo to use, pass site name as default.
          else {
            $site_footer['site_name'] = $this->configFactory->get('system.site')->get('name');
          }
        }
      }
      // When select no logo.
      else {

        // When select no logo, add or set site_name variable.
        $config = $this->configFactory->get('system.site');
        $site_footer['site_name'] = $config->get('name');
      }

      // Load in current node.
      $current_node = $this->currentRouteMatch->getParameter('node');

      // When viewing revisions current node comes as a string, in that case
      // load the node entity.
      if (is_string($current_node)) {
        $current_node = $this->entityTypeManager->getStorage('node')->load($current_node);
      }

      // If the current node is present, process it.
      if (isset($current_node)) {

        // Check if the user is logged in and if so display
        // content moderation for the local site footer.
        $logged_in = $this->accountProxy->isAuthenticated();

        // If logged in and on a site footer, load in the local menu tasks.
        if ($logged_in && $current_node->bundle() == "uw_ct_site_footer") {

          $menu_local_tasks = $this->localTaskManager
            ->getLocalTasks($this->currentRouteMatch->getRouteName(), 0);

          // Unset the render sections (throws error if set).
          unset($menu_local_tasks['route_name']);
          unset($menu_local_tasks['cacheability']);

          // Assign to the block to be rendered as local menu tasks
          // in the correct spot.
          $site_footer['primary'] = $menu_local_tasks;
        }
      }
    }

    // Add the home link to the site footer.
    $site_footer['home_link'] = Url::fromRoute('<front>', [], ['absolute' => 'true'])->toString();

    return [
      '#theme' => 'uw_site_footer',
      '#site_footer' => $site_footer,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheTags() {

    return Cache::mergeTags(parent::getCacheTags(), ['site_footer']);
  }

  /**
   * {@inheritDoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager,
    CurrentPathStack $currentPathStack,
    CurrentRouteMatch $currentRouteMatch,
    AccountProxyInterface $accountProxy,
    LocalTaskManagerInterface $localTaskManager,
    ConfigFactoryInterface $configFactory,
    Connection $database,
    UWServiceInterface $uwService,
    UWBrandLogoInterface $brandLogo
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entityTypeManager;
    $this->currentPathStack = $currentPathStack;
    $this->currentRouteMatch = $currentRouteMatch;
    $this->accountProxy = $accountProxy;
    $this->localTaskManager = $localTaskManager;
    $this->configFactory = $configFactory;
    $this->database = $database;
    $this->uwService = $uwService;
    $this->brandLogo = $brandLogo;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('path.current'),
      $container->get('current_route_match'),
      $container->get('current_user'),
      $container->get('plugin.manager.menu.local_task'),
      $container->get('config.factory'),
      $container->get('database'),
      $container->get('uw_cfg_common.uw_service'),
      $container->get('uw_ct_site_footer.logo'),
    );
  }

}
