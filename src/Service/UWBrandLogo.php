<?php

namespace Drupal\uw_ct_site_footer\Service;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\State\State;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\Exception\ClientException;

/**
 * Service for getting logo.
 */
class UWBrandLogo implements UWBrandLogoInterface {

  // Using trait to get logger.
  use LoggerChannelTrait;

  // String translation trait (t function).
  use StringTranslationTrait;

  /**
   * Where to store local copy of the brand logo.
   */
  private const BRAND_LOGO_LOCAL_FOLDER = 'public://logos/';

  /**
   * How long (in seconds) local logo will be considered valid. 21600 is 6h.
   */
  private const BRAND_LOGO_LOCAL_LIFESPAN = 21600;

  /**
   * External server that hold site footer logos.
   */
  private const BRAND_SITE_FOOTER_LOGO_URL = 'https://uwaterloo.ca/brand/api/v1.0/uw-logos-site-footer/';

  /**
   * State API id for the logo stuff.
   */
  private const LOGO_STATE_ID = 'uw-site-footer-logo';

  /**
   * Default constructor using property promotion.
   *
   * @param \Drupal\Core\Http\ClientFactory $clientFactory
   *   HTTP Client from the core.
   * @param \Drupal\Core\State\State $state
   *   State service, for getting/setting variables.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   File system service, for file/folder manipulation.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   File Url Generator from the core, using to generate urls.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Time service, replacement for REQUEST_TIME.
   */
  public function __construct(
    protected ClientFactory $clientFactory,
    protected State $state,
    protected FileSystemInterface $fileSystem,
    protected FileUrlGeneratorInterface $fileUrlGenerator,
    protected TimeInterface $time,
  ) {}

  /**
   * Retrieves the brand site logo for a given name.
   *
   * @param string $name
   *   The logo ID.
   *
   * @return array
   *   The logo information as an associative array.
   */
  private function getBrandSiteFooterLogo(string $name): array {
    $logo = [];

    $client = $this->clientFactory->fromOptions(['base_uri' => self::BRAND_SITE_FOOTER_LOGO_URL]);
    try {
      $response = $client->get($name);

      // If brand site returns info we need, use it.
      if ($response->getStatusCode() === 200) {
        $content = Json::decode($response->getBody()?->getContents());
        $logo = $this->getBrandSiteFooterLogoParse($content);
      }
    }
    catch (ClientException | \Exception $e) {
      $this->getLogger('UWService')->warning(
        $this->t(
          "Brand site returned code: @code while requesting logo id: @logo. <br><br> @message",
          [
            '@code' => $e->getCode(),
            '@logo' => $name,
            '@message' => $e->getMessage(),
          ]
        )
      );
    }

    return $logo;
  }

  /**
   * Parses the content returned by the brand site to extract logo information.
   *
   * @param array $content
   *   The content returned by the brand site.
   *
   * @return array
   *   The parsed logo information.
   */
  private function getBrandSiteFooterLogoParse(array $content): array {
    $logo = [];

    if (!empty($content)) {
      // If content has more than one result in array, use first one.
      $content = current($content);

      [
        'site_footer_link' => $logo_link,
        'site_footer_logo' => [
          'src' => $logo_src,
          'alt' => $logo_alt,
        ],
      ] = $content;

      if ($logo_link && $logo_src && $logo_alt) {
        $logo['logo_link'] = $logo_link;
        $logo['logo_url'] = $logo_src;
        $logo['logo_alt_text'] = $logo_alt;
      }

    }

    return $logo;
  }

  /**
   * Try to load site footer logo from the local.
   */
  private function getLocalSiteFooterLogo(): array {
    $logo = $this->state->get(self::LOGO_STATE_ID);

    if (!empty($logo) && !empty($logo['logo_url'])) {
      $local_logo_source = self::BRAND_LOGO_LOCAL_FOLDER . $this->getLocalSiteFooterLogoFilename($logo['logo_url']);

      // Generate logo local url.
      if ($local_logo_url = $this->fileUrlGenerator->generateString($local_logo_source)) {

        // Return only fields theme layer needs. Not returning
        // updated timestamp, or logo name/id.
        return [
          'logo_url' => $local_logo_url,
          'logo_link' => $logo['logo_link'],
          'logo_alt_text' => $logo['logo_alt_text'],
        ];
      }
    }

    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function getSiteFooterLogo(string $name): array {

    // Check brand site first. Short-circuit if found.
    if (!empty($logo = $this->getBrandSiteFooterLogo($name))) {

      // Set a local copy of the logo if needed.
      $this->setLocalSiteFooterLogo($name, $logo);
      return $logo;
    }

    // If brand site is unreachable or something is wrong with it,
    // try loading site footer logo from local. If this is not found,
    // let SiteFooter set site name to be used instead of a logo.
    return $this->getLocalSiteFooterLogo();
  }

  /**
   * Set the local site footer logo.
   *
   * By default, /tmp folder contents are cleared every 6 hours by cron.
   * But that is configurable: drush cget system.file temporary_maximum_age.
   *
   * @param string $name
   *   Logo id/name.
   * @param array $logo
   *   The logo details to set. Should contain the logo image, name, and
   *   description.
   */
  private function setLocalSiteFooterLogo(string $name, array $logo): void {
    $log_state = $this->state->get(self::LOGO_STATE_ID);

    // If there is not saved logo data, create it.
    if (!$log_state) {
      $this->updateLogoState($name, $logo);
      return;
    }

    // If logo name (id) is not the same, then update local logo regardless.
    if ($name !== $log_state['name']) {
      $this->updateLogoState($name, $logo);
      return;
    }

    // At this point, brand site works, we just need to make sure we
    // have the latest logo stored locally.
    if (!isset($log_state['updated']) || (($log_state['updated'] + self::BRAND_LOGO_LOCAL_LIFESPAN) < $this->time->getRequestTime())) {
      $this->updateLogoState($name, $logo);
    }
  }

  /**
   * Updates the state of the logo.
   *
   * @param string $name
   *   The new name of the logo.
   * @param array $logo
   *   The logo array to be updated.
   */
  private function updateLogoState(string $name, array &$logo): void {
    $logo['updated'] = $this->time->getRequestTime();
    $logo['name'] = $name;
    if ($this->setLocalSiteFooterLogoCreateLocalCopy($logo)) {
      $this->state->set(self::LOGO_STATE_ID, $logo);
    }
  }

  /**
   * Gets the filename for the local site footer logo.
   *
   * @param string $url
   *   The URL of the logo file.
   *
   * @return string|null
   *   The filename for the local site footer logo, or null if the
   *   file extension is not supported.
   */
  private function getLocalSiteFooterLogoFilename(string $url): ?string {

    ['path' => $path] = UrlHelper::parse($url);

    $url_parts = explode('/', parse_url($path, PHP_URL_PATH));

    if ($filename = array_pop($url_parts)) {
      return $filename;
    }

    return NULL;
  }

  /**
   * Creates/saves local logo copy.
   *
   * @param array $logo
   *   Logo array with url, name, and alt text.
   */
  private function setLocalSiteFooterLogoCreateLocalCopy(array $logo): bool {
    $destination_folder = self::BRAND_LOGO_LOCAL_FOLDER;

    if ($this->fileSystem->prepareDirectory($destination_folder, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      $filename = $this->getLocalSiteFooterLogoFilename($logo['logo_url']);

      try {
        $this->clientFactory->fromOptions()->get($logo['logo_url'], ['sink' => $destination_folder . $filename]);

        return TRUE;
      }
      catch (ClientException | \Exception $e) {
        $this->getLogger('UWService')
          ->error($this->t('Error: @message', ['@message' => $e->getMessage()]));
      }
    }

    return FALSE;
  }

}
