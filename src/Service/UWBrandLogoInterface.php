<?php

namespace Drupal\uw_ct_site_footer\Service;

/**
 * Service for getting brand logo from remote/local.
 */
interface UWBrandLogoInterface {

  /**
   * Get image source, name, and description for site footer logo.
   *
   * @param string $name
   *   Logo name.
   *
   * @return array
   *   Details containing logo and other meta-data.
   */
  public function getSiteFooterLogo(string $name): array;

}
