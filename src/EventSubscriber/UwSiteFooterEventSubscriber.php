<?php

namespace Drupal\uw_ct_site_footer\EventSubscriber;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\core_event_dispatcher\EntityHookEvents;
use Drupal\core_event_dispatcher\Event\Entity\EntityDeleteEvent;
use Drupal\core_event_dispatcher\Event\Entity\EntityInsertEvent;
use Drupal\core_event_dispatcher\Event\Entity\EntityLoadEvent;
use Drupal\core_event_dispatcher\Event\Entity\EntityUpdateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UwSiteFooterEventSubscriber.
 *
 * UW site footer event subscriber.
 *
 * @package Drupal\uw_ct_site_footer\EventSubscriber
 */
class UwSiteFooterEventSubscriber implements EventSubscriberInterface {

  /**
   * Entity type manager from core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * URL generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      EntityHookEvents::ENTITY_INSERT => 'entityInsert',
      EntityHookEvents::ENTITY_LOAD => 'entityLoad',
      EntityHookEvents::ENTITY_UPDATE => 'entityUpdate',
      EntityHookEvents::ENTITY_DELETE => 'entityDelete',
    ];
  }

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Type Manager, used to load entities.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $urlGenerator
   *   Url Generator used to create url.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The user account.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, UrlGeneratorInterface $urlGenerator, AccountProxyInterface $account) {
    $this->entityTypeManager = $entityTypeManager;
    $this->urlGenerator = $urlGenerator;
    $this->account = $account;
  }

  /**
   * Event (site footer) load event.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityLoadEvent $event
   *   Fired event from hook_event_dispatcher.
   */
  public function entityLoad(EntityLoadEvent $event) {

    // Check for entity type id first, to avoid circular requests. Checking
    // permission will trigger another request for entity load, and web server
    // will error out after so many requests. Act only on entity view.
    if ($event->getEntityTypeId() == 'entity_view_display') {

      // Revisions of site footer are visible only to content authors/editors
      // roles other roles including anonymous will not have it.
      if ($this->account->hasPermission('view uw_ct_site_footer revisions')) {

        // Load all the entities.
        $entities = $event->getEntities();

        // Get the first one, since we are only allowed one
        // site footer, it will be the first element in the
        // entities array.
        $entity = array_values($entities)[0];

        // One last check to ensure that we are on a site footer,
        // and if so invalid the cache for site footer.
        if ($entity->get('id') == 'node.uw_ct_site_footer.default') {
          Cache::invalidateTags(['site_footer']);
        }
      }
    }
    // Bypass site footer cache for all users with site footer revisions
    // permission. This will disable cache for administrators and site managers.
    elseif ($event->getEntityTypeId() === 'node' && $this->account->hasPermission('view uw_ct_site_footer revisions')) {
      Cache::invalidateTags(['site_footer']);
    }
  }

  /**
   * Event (site footer) create/insert event.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityInsertEvent $event
   *   Fired event from hook_event_dispatcher.
   */
  public function entityInsert(EntityInsertEvent $event) {
    $this->clearCache($event->getEntity());
  }

  /**
   * Entity (site footer) update.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityUpdateEvent $event
   *   Fired event from hook_event_dispatcher.
   */
  public function entityUpdate(EntityUpdateEvent $event) {
    $this->clearCache($event->getEntity());
  }

  /**
   * Entity delete.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityDeleteEvent $event
   *   Fired event from hook_event_dispatcher.
   */
  public function entityDelete(EntityDeleteEvent $event) {
    $this->clearCache($event->getEntity());
  }

  /**
   * Clears global cache.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity from event.
   */
  protected function clearCache(EntityInterface $entity) {
    // Need to check bundle, since moderation entity is available too.
    if ($entity->bundle() === 'uw_ct_site_footer') {
      // Clear specific cache tag, only for site footer.
      Cache::invalidateTags(['site_footer']);
    }
  }

}
